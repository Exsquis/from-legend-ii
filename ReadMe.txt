---------------------
ROOT
---------------------

Here are all the folders and files for the project.

_OLD: contains the old folders and files of the Project.

1- Common: contains all the common files for the team (Concept, Unity Project, Communication...)

2- Programmation: created as empty, it can contain documents from programmers (.cpp for example).

3- Game Design: contains all the Game Design & Level Design documents of the game.

4- Game Arts: contains all the source documents (.psd, .ma, .JPEG, etc...) for the pre-production & production from the Art Team. 

5- Sound Design: contains all the sounds & musics for the game. 


Note: The 'Common' folder only contains the exported files. All the source files must be found in their respective section.
Note2: all the folders contain a "x-" notation, in order to organize them. However, the project keeps its original folders (without any number).
Note3: giving good names to the documents in the Git, please consult the document 'Legend_Nomenclature'. Path: 1- Common\4- Communication\1- Internal
