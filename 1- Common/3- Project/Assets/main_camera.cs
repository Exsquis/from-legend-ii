using UnityEngine;
using System.Collections;

public class main_camera : MonoBehaviour {


	private string __blockDirection; //Used to indicate which side of the camera is blocked
	[HideInInspector]
	public GameObject CameraLeftBlocker;//Game object used to limit left movement of camera
	[HideInInspector]
	public GameObject CameraRightBlocker;//Game object used to limit right movement of camera
	private bool __isMoving; // Define if the camera is moving
	private GameObject __player;// Define The Gameobject to follow
	private Vector3 __target;//Define the target to reach while following an object
	private Vector3 __origin;//Define the origin of the movement while following an object
	[HideInInspector]
	public float TranslateSpeed; // used to stock the current camera tranlate speed 
	[HideInInspector]
	public float TranslateSpeedIncr; // Define camera follow speed incrementation on time when the object to follow is moving
	[HideInInspector]
	public float TranslateSpeedDecr; // Define camera follow speed decrementation on time when the object to follow is not moving
	[HideInInspector]
	public float TranslateBaseSpeed; // Define the base speed of camera translation
	[HideInInspector]
	public float TranslateMaxSpeed; // Define the maximum speed of camera translation
	[HideInInspector]
	public int CameraDistanceZ; // Defin the camera distance
	[HideInInspector]
	public int CameraYJump; // Define the camera position on Y axis
	[HideInInspector]
	public int CameraYGround; // Define the camera position on Y axis
	[HideInInspector]
	public int CameraYSpeed; // Define the camera Speed on Y axis
	private Main_Character __characterScript; // used to access the main character script

	void Awake()
	{
		this.CameraLeftBlocker = GameObject.FindGameObjectWithTag ("CameraLeftBlocker"); // AUTOMATIC : Get camera left blocker in scene
		this.CameraRightBlocker = GameObject.FindGameObjectWithTag("CameraRightBlocker"); // AUTOMATIC : Get camera right blocker in scene
	}

	// Use this for initialization
	void Start () {
		//POSITION PARAMETERS
		this.CameraYSpeed = 5;
		this.CameraYGround = 5; // Define position on Y axis while grounded
		this.CameraYJump = 2; // Define Y position on Y axis while airborne
		this.CameraDistanceZ = 7; // Define camera distance
		Camera.main.orthographicSize = this.CameraDistanceZ; // Set camera distance
		//FOLLOW PARAMETERS
		this.TranslateBaseSpeed = 5; // Set Translation base speed
		this.TranslateSpeedIncr = 10; // Set camera translate incrementation speed
		this.TranslateSpeedDecr = 10; // Set camera translate decrementation speed
		this.TranslateMaxSpeed = 30; // Set camera maximum speed
		// Set other Variables
		this.__isMoving = true; // Set camera as moving
		this.TranslateSpeed = this.TranslateBaseSpeed; // Set translate speed to the base speed
		this.__player = GameObject.FindGameObjectWithTag ("Player"); // AUTOMATIC : find player in scene
		this.__target = new Vector3 (this.__player.transform.position.x, this.__player.transform.position.y + this.CameraYGround, -10); // Set amera target vector
		this.__origin = new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y, -10); // Set camera Origin Vector
		this.__characterScript = this.__player.GetComponent<Main_Character> (); // AUTOMATIC : Get main character script.
	}
	
	// Update is called once per frame
	void Update () {
		//CHECK COLLISION WITH STAGE'S BORDERS
		//LEFT
		if(this.gameObject.transform.position.x <= this.CameraLeftBlocker.transform.position.x && this.__characterScript.xSpeed < 0) // If camera position on x axis < left blocker position on x axis and player's current speed < 0
		{
			this.__blockDirection = "Left"; // COnsider that camera is blocked on left side
			this.__isMoving = false; // Disable camera movement
		}
		else if (this.__characterScript.xSpeed > 0 && this.gameObject.transform.position.x <= this.__player.transform.position.x && this.__blockDirection == "Left")//If the main character speed on x axis > 0 and the camera position on x axis <= main character position on x axis and camera is blocked on left side
		{
			this.__isMoving = true; // Enable camera movement
		}

		//RIGHT
		if(this.gameObject.transform.position.x >= this.CameraRightBlocker.transform.position.x && this.__characterScript.xSpeed > 0)// If camera position on x axis >= right blocker position on x axis and player's current speed > 0
		{
			this.__blockDirection = "Right"; // COnsider that camera is blocked on right side
			this.__isMoving = false; // Disable Camera Movement
		}
		else if (this.__characterScript.xSpeed < 0 && this.gameObject.transform.position.x >= this.__player.transform.position.x && this.__blockDirection == "Right")//If the main character speed on x axis < 0 and the camera position on x axis >= main character position on x axis and camera is blocked on right side
		{
			this.__isMoving = true; // Enable Camera Movement
		}




if( this.__isMoving)// If the camera is considered as moving
		{
		//FOR DODGE
		if(this.__characterScript.__state == 2) // if character state = Dodge
		{
			this.TranslateSpeed = this.TranslateMaxSpeed; // Set camera translate speed to maximum to follow the dodge properly
		}

		//FOR JUMP
		if(!this.__characterScript.grounded) // If character is not grounded enable y axis camera behavior
		{
			this.TranslateSpeed = this.CameraYSpeed; // Set camera speed to a custom value to follow properly on y axis
			this.__target = new Vector3 (this.__player.transform.position.x, this.__player.transform.position.y + this.CameraYJump, -10); // Adjust camera target translation on y axis to match properly Y axis move
			this.__origin = new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y, -10); // Adjust camera origin translation on y axis to match properly Y axis move
		}

		else // If character is grounded and not dodging enable Camera normal behavior
		{
				this.__target = new Vector3 (this.__player.transform.position.x, this.__player.transform.position.y +this.CameraYGround, -10); //Adjust camera target translation on y axis to match properly X axis move
				this.__origin = new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y, -10); //Adjust camera origin translation on y axis to match properly X axis move
		}

		this.gameObject.transform.position = Vector3.Lerp(this.__origin,this.__target, this.TranslateSpeed*Time.deltaTime); // every milliseconds (scaled on frame rate) move camera from its origin (itself) position to the target position (player) according to the defined tranlate speed

			//USED FOR INCREMENTATION OF TRANSLATION SPEED
			if(this.__characterScript.xSpeed > 1 && this.TranslateSpeed < this.TranslateMaxSpeed || this.__characterScript.xSpeed < -1 && this.TranslateSpeed < this.TranslateMaxSpeed ) //If character speed on x axis > 1 and camera translate speed < camera translate maximum speed or If character speed on x axis < -1 and camera translate speed < camera translate maximum speed
		{
			this.TranslateSpeed += this.TranslateSpeedIncr * Time.deltaTime; // Increment camera translate speed by defined incrementation speed every milliseconds (according to the frame rate)
		}
		else if ( this.__characterScript.__state == 0 && this.TranslateSpeed > this.TranslateBaseSpeed) // If character is n stand state andcamera translate speed > its base speed
		{
				this.TranslateSpeed -= this.TranslateSpeedDecr*Time.deltaTime ;// Decrement camera translate speed by defined decrementation speed every milliseconds (according to the frame rate)
				 
		}
		}

		else
		{
			if(this.__characterScript.grounded)
			{
				this.__origin = new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y, -10); //Adjust camera origin translation on y axis to match properly X axis move
				this.__target = this.__target = new Vector3 (this.transform.position.x, this.__player.transform.position.y + this.CameraYGround, -10); // Set amera target vector
				this.gameObject.transform.position = Vector3.Lerp(this.__origin,this.__target, this.TranslateSpeed*Time.deltaTime); // every milliseconds (scaled on frame rate) move camera from its origin (itself) position to the target position (player) according to the defined tranlate speed
			}
			else if(!this.__characterScript.grounded)
			{
				this.__origin = new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y, -10); //Adjust camera origin translation on y axis to match properly X axis move
				this.__target = this.__target = new Vector3 (this.transform.position.x, this.__player.transform.position.y + this.CameraYJump, -10); // Set amera target vector
				this.gameObject.transform.position = Vector3.Lerp(this.__origin,this.__target, this.TranslateSpeed*Time.deltaTime); // every milliseconds (scaled on frame rate) move camera from its origin (itself) position to the target position (player) according to the defined tranlate speed
			}

		}


	}


}
