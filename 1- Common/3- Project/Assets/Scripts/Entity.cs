﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {
	///////REFERENCE OBJECTS
	[HideInInspector]
	public GameObject GameMaster;
	[HideInInspector]
	public InputConfig InputConfigScript;
	[HideInInspector]
	public Rigidbody2D Rigidbody;
	[HideInInspector]
	public float GravityScale;
	///////FX VARIABLES
	public GameObject FxPlayer;// Define the object containing all FX emiters
	public GameObject FxFootPosition; // Define the FX foot emitter's position (for fx like jump)
	[HideInInspector]
	public string FxAnimation; // used to call an fx
	[HideInInspector]
	public bool SnapFx; // If true snap used FX to the source (character or enemy) else do not snap
	[HideInInspector]
	public bool FxFootHasPlayed; // Define if a foot fx has already be played in a state
	// STATE AND BEHAVIOR VARIABLES
	[HideInInspector]
	public bool HasJumped;// Defines if character has jumped already
	[HideInInspector]
	public bool HasDoubleJumped;// Defines if character has double jumped already
	[HideInInspector]
	public float __duration; // Define a state duration
	[HideInInspector]
	public int __direction; // Define character's current direction, -1 for left, +1 for right
	[HideInInspector]
	public int ComboType; // Define which combo is executed
	[HideInInspector]
	public int __comboState; // Define the position in the combo string (1st strike, 2nd strike, etc...)
	[HideInInspector]
	public int __state; // Define the current state
	[HideInInspector]
	public int __nextState; // define the next state
	[HideInInspector]
	public float __counter; // Counter used for state duration
	[HideInInspector]
	public float __comboCounter; // Counter used for combo state duration
	[HideInInspector]
	public bool grounded = false; // Define if the character is gorunded or not
	[HideInInspector]
	public bool IsInvincible = false; // Define if the character is invincible
	//ANIMATION VARIABLES
	public GameObject[] Animations;// Define character's animations list, position in the array define the state in which animation is used (fe: animation[O] is stand animation)
	//SOUND VARIABLES
	public AudioClip[] Sounds;// Define character's sounds list, position in the array define the state in which sound is used (fe: sound[2] is dodge sound)
	[HideInInspector]
	// PROPERTIES VARIABLES
	public int HP; // Define character life , 2 = 1 full heart, 1 = 1/2 heart
	[HideInInspector]
	public float xSpeed; // Define character's speed on x axis
	[HideInInspector]
	public float ySpeed; // Define character's speed on y axis
	public BoxCollider2D Weapon; // Defin the used box as a weapon

	public void GetInfo()
	{
		this.Rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
	}

	// Use this for initialization
	public void SetInfo () 
	{
		this.GravityScale = this.Rigidbody.gravityScale;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate()
	{

	}

	//USED TO DEFINE IF THE CHARACTER IS GROUNDED OR NOT
	void OnCollisionEnter2D(Collision2D col) // If the Hit box character  collides....
	{
		if(col.gameObject.tag == "Ground") // ...And this collider is the ground
		{
			this.grounded = true; // The character is grounded
			this.HasDoubleJumped = false; // reset double jump
			this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;// Reset impulsion
		}
	}
	
	void OnCollisionExit2D(Collision2D col) // If the character gets out of a collision....
	{
		if(col.gameObject.tag == "Ground") // ...And this collisions was the floor
		{
			this.grounded = false; // The character is NotRenamedAttribute grounded
		}
	}
	//////////////////////

	// FUNCTION TO PLAY FOOT FX
	public void PlayFootFX()
	{
		if(this.FxAnimation != null && !this.FxFootHasPlayed) // If a FX animation has been called and did not play for this state
		{
			this.FxFootHasPlayed = true; // the FX has played
			GameObject clone; // define a GO to contain the fx
			clone = Instantiate (FxPlayer, this.FxFootPosition.transform.position, this.FxFootPosition.transform.rotation)as GameObject;// INSTANTIATE the fx player object at the fxfoot position on X,Yand Z axis and apply its current rotation
			if(this.__direction == 1) // If the character is facing right
			{
				clone.transform.rotation = new Quaternion (0,0,0,0);//the fx object rotation is facing right
			}
			else if (this.__direction == -1) // If the character is facing left
			{
				clone.transform.rotation = new Quaternion (0,180,0,0);// the FX object rotation is facing left
			}

			if(this.SnapFx) // the snap parameter is set to true
			{
				clone.transform.parent = this.FxFootPosition.transform; // The fx GO snaps to the character
			}
			clone.GetComponent<Animator> ().Play (this.FxAnimation);// Play the fx from the gameobject
			Destroy(clone,2);// destroy the fx after 2 seconds
		}
	}

	//FUNCTION USED TO CHANGE STATE
	public void ChangeState()
	{
		// RESET INVINCIBILITY
		this.IsInvincible = false;

		//RESET FX PLAYER
		this.FxAnimation = null; // Reset fx animation id
		this.SnapFx = false;// reset fx snap
		this.FxFootHasPlayed = false; //  the fx player is not considered as played anymore

		//RESET COMBO TYPE and timer
		this.ComboType = 0;// reset combo type
		this.__comboState = 0; // reset combo state
		this.__comboCounter = 0; // reset combo counter

		//RESET WEAPON
		this.Weapon.enabled = false; // disable weapon

		//DISABLE PREVIOUS SOUND AND ANIMATION + RESET AUDIO LOOP COMPONENT TO FALSE
		if(this.GetComponent<AudioSource>().loop == true) // If the audio source is looping
		{
			this.GetComponent<AudioSource>().Stop ();// stop the current sound
		}
		this.GetComponent<AudioSource>().loop = false; // switch sound loop to false

		//Handle Animations for next state
		this.Animations[this.__state].SetActive(false); // Disable current state's animation
		this.__state = this.__nextState; // set current state to the next one

		//RESET GRAVITY
		this.Rigidbody.gravityScale = this.GravityScale;

		//RESET JUMP
		this.HasJumped = false;


		// PLAY STATE SOUND
		if(this.Sounds[this.__state] != null) // if  the current state has an assigned sound
		{
			this.GetComponent<AudioSource>().clip = this.Sounds [this.__state]; // set to audio source the matching sound
			this.GetComponent<AudioSource>().Play (); // Play the assigned sound
		}
		// PLAY ANIMATION
		if(this.Animations[this.__state] != null) // If there is an existing animation for the current state
		{
			this.Animations[this.__state].SetActive(true); // Play assigned animation
		}
		this.__counter = 0;// Reset counter;
	}

}
