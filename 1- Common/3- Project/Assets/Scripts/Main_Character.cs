using UnityEngine;
using System.Collections;

public class Main_Character : Entity {
	[HideInInspector]
	public bool IsMoving; // Define if character is moving
	[HideInInspector]
	public bool CanDoubleJump; // Define if character Can double jump
	[HideInInspector]
	public bool CanAirDodge; // Define if character Can Air DOdge
	void Awake()
	{
		GetInfo ();
		this.GameMaster = GameObject.FindGameObjectWithTag ("GameMaster");
	}
	// Use this for initialization
	void Start () {
		SetInfo ();
		// Defines enabled capacities
		this.CanDoubleJump = false;
		this.CanAirDodge = true;
		// Behavior variables
		this.IsMoving = false;
		this.__nextState = 6;
		ChangeState ();
		this.HasJumped = false;
		this.InputConfigScript = this.GameMaster.GetComponent<InputConfig> ();
	}

	void Update()
	{
		this.__counter += Time.deltaTime;// Increment counter at each millisecond (according to framerate)

		if(Input.GetAxis ("Horizontal") <= this.InputConfigScript.JoystickMoveTolerance*-1 || Input.GetAxis ("Horizontal") >= this.InputConfigScript.JoystickMoveTolerance) // if joystick is pushed
		{
			this.IsMoving = true; // define that character is moving
		}
		else if (Input.GetAxis ("Horizontal") == 0)
		{
			this.IsMoving = false;
		}
	}
	void FixedUpdate()
		{
		//DEFINE MOVE AND GRAVITY
		this.GetComponent<Rigidbody2D>().velocity = (new Vector2 (this.xSpeed, this.GetComponent<Rigidbody2D>().velocity.y)); // apply velocity and gravity at each millisecond

		//DEFINE DIRECTION
		if(Input.GetAxis ("Dodge") > 0 && this.__state != 2 && this.__state != 11 && Input.GetButtonDown("Dodge"))
		{
			__direction = 1;
			this.gameObject.transform.localScale = new Vector3 (1,1,1);
		}
		else if(Input.GetAxis ("Dodge") < 0 && this.__state != 2 && this.__state != 11 && Input.GetButtonDown("Dodge"))
		{
			__direction = -1;
			this.gameObject.transform.localScale = new Vector3 (-1,1,1);
		}
		else if(Input.GetAxis ("Horizontal") > 0 && this.__state != 2 && this.__state != 11)
		{
			__direction = 1;
			this.gameObject.transform.localScale = new Vector3 (1,1,1);
		}
		else if(Input.GetAxis ("Horizontal") < 0 && this.__state != 2 && this.__state != 11)
		{
			__direction = -1;
			this.gameObject.transform.localScale = new Vector3 (-1,1,1);
		}

		
		
		///////////////////////////////////////////////////////////////////STATES///////////////////////////////////////////////////////////////////////////////////////////////
		//ANY STATE
		if(Input.GetButtonDown("Jump") && this.grounded && this.__state != 2 && this.HasJumped == false) // JUMP
		{
			this.__nextState = 3; // JUMP
			ChangeState();
		}

		//STAND
		if (this.__nextState == 0)
		{

			//behavior//////////////////////
			this.xSpeed = 0 * Time.deltaTime;
			///////////////////////////////

			//State change/////////////////////////////
			if(this.IsMoving)//RUN
			{
				this.__nextState = 8; // RUN OPEN FAN
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Jump"))//JUMP
			{
				
				this.__nextState = 3; // JUMP
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Dodge"))//DODGE
			{
				
				this.__nextState = 2; // DODGE
				ChangeState();
			}

			else if(this.__counter >= 2)//Fan Out
			{
				
				this.__nextState = 5; // Fan Out
				ChangeState();
			}
			else if(Input.GetButtonDown("Attack")) // ATTACK
			{
				this.ComboType = 1;
			}
			////////////////////////////////////////
		}
		
		
		//RUN 
		else if (this.__nextState == 1) 
		{ 

			//behavior//////////////////////
			this.xSpeed = (__direction* 850) * Time.deltaTime;
			this.GetComponent<AudioSource>().loop = true;

			///////////////////////////////

			//Change State//////////////////////
			if(!this.IsMoving)//RUN OUT
			{
				this.__nextState = 7;//RUN OUT
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Dodge"))//DODGE
			{
				
				this.__nextState = 2; // DODGE
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Jump"))//JUMP
			{
				
				this.__nextState = 3; // JUMP
				ChangeState();
			}
			else if(Input.GetButtonDown("Attack")) // ATTACK
			{
				this.ComboType = 1;
			}
			//////////////////////////////////////
			
		}
		
		//DODGE
		else if (this.__nextState == 2)
		{ 
			//behavior//////////////////////
			this.__duration = 0.3f;
			this.xSpeed = (__direction* 1500) * Time.deltaTime;
			///////////////////////////////

			//Change State//////////////////////
			if(this.__counter >= this.__duration && this.IsMoving)//RUN
			{
				
				this.__nextState = 1;//RUN 
				ChangeState();
			}
			else if(this.__counter >= this.__duration)//Run_Out
			{
				
				this.__nextState = 7;//RUN OUT
				ChangeState();
			}
			//////////////////////////////////
		}
		
		// JUMP
		else if(this.__nextState == 3)
		{
			if(this.HasJumped == false)
			{
				this.HasJumped = true;
				this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1250);
			}
			//behavior//////////////////////
			this.FxAnimation = "fx_fulin_jump";
			PlayFootFX();
			this.xSpeed = this.xSpeed = (Input.GetAxis("Horizontal")* 500) * Time.deltaTime;
			///////////////////////////////

			//Change State////////////////
			if(this.GetComponent<Rigidbody2D>().velocity.y < 0)//FALL
			{
				this.__nextState = 4;//FALL
				ChangeState();
			}

			if(Input.GetButtonDown("Jump") && !this.grounded && this.__state != 2 && this.HasDoubleJumped == false && this.CanDoubleJump) // DOUBLE JUMP
			{
				this.__nextState = 10; // DOUBLE JUMP
				ChangeState();
			}

			if(Input.GetButtonDown ("Dodge") && this.CanAirDodge)// AIR DODGE
			{
				
				this.__nextState = 11; // AIR DODGE
				ChangeState();
			}
			//////////////////////////////
			
			
		}

		//FALL
		else if(this.__nextState == 4)
		{
			//behavior//////////////////////
			this.xSpeed = (Input.GetAxis("Horizontal")* 500) * Time.deltaTime;
			if(Input.GetAxis("Vertical") < -this.InputConfigScript.JoystickMoveTolerance)// INPUT DOWN
			{
				this.Rigidbody.gravityScale = this.GravityScale *3; //EMPOWER GRAVITY
			}
			///////////////////////////////

			//Change State/////////////////
			if(this.grounded == true && this.IsMoving)
			{
				this.__nextState = 1;//RUN
				this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				ChangeState();
			}	
			else if(this.grounded == true && !this.IsMoving)
			{
				this.__nextState = 9;//COMBO OUT
				this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				ChangeState();
			}
			if(Input.GetButtonDown("Jump") && !this.grounded && this.__state != 2 && this.HasDoubleJumped == false && this.CanDoubleJump) // DOUBLE JUMP
			{
				this.__nextState = 10; // DOUBLE JUMP
				ChangeState();
			}
			if(Input.GetButtonDown ("Dodge") && this.CanAirDodge)// AIR DODGE
			{
				
				this.__nextState = 11; // AIR DODGE
				ChangeState();
			}
			//////////////////////////////
			
		}

		//FAN OUT
		else if(this.__nextState == 5) 
		{
			//behavior//////////////////////
			this.xSpeed = 0 * Time.deltaTime;
			///////////////////////////////

			// CHange State///////////////////////
			if(this.IsMoving)//RUN
			{
				
				this.__nextState = 1; // RUN 
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Jump"))//JUMP
			{
				
				this.__nextState = 3; // JUMP
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Dodge"))//DODGE
			{
				
				this.__nextState = 2; // DODGE
				ChangeState();
			}
			
			else if(this.__counter >= 0.5)//STAND NO FAN
			{
				
				this.__nextState = 6; // Fan Out
				ChangeState();
			}
			else if(Input.GetButtonDown("Attack")) // ATTACK
			{
				this.ComboType = 1;
			}
			//////////////////////////////////////

			
		}

		//STAND NO FAN
		else if (this.__nextState == 6)
		{
			//behavior//////////////////////
			this.xSpeed = 0 * Time.deltaTime;
			///////////////////////////////

			//Change State///////////////////////////
			if(this.IsMoving)//RUN
			{
				
				this.__nextState = 8; // RUN OPEN FAN
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Jump"))//JUMP
			{
				
				this.__nextState = 3; // JUMP
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Dodge"))//DODGE
			{
				
				this.__nextState = 2; // DODGE
				ChangeState();
			}
			else if(Input.GetButtonDown("Attack")) // ATTACK
			{
				this.ComboType = 1;
			}
			/////////////////////////////////////
		}

		//RUN OUT
		else if (this.__nextState == 7)
		{
			//behavior//////////////////////
			this.xSpeed = (__direction*150) * Time.deltaTime;
			///////////////////////////////

			//Change State///////////////////
			if(this.IsMoving)//RUN
			{
				
				this.__nextState = 8; // RUN OPEN FAN
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Jump"))//JUMP
			{
				
				this.__nextState = 3; // JUMP
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Dodge"))//DODGE
			{
				
				this.__nextState = 2; // DODGE
				ChangeState();
			}

			else if(this.__counter >= 0.35) // STAND
			{
				
				this.__nextState = 0; // STAND
				ChangeState();
			}
			else if(Input.GetButtonDown("Attack")) // ATTACK
			{
				this.ComboType = 1;
			}
			////////////////////////////////////
			
		}

		//RUN OPEN FAN//////////////////
		else if (this.__nextState == 8)
		{ 
			//behavior//////////////////////
			this.xSpeed = (__direction* 850) * Time.deltaTime;
			///////////////////////////////

			//Change State////////////////
			if(!this.IsMoving)//STAND 
			{	
				this.__nextState = 0;//STAND
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Dodge"))//DODGE
			{
				
				this.__nextState = 2; // DODGE
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Jump"))//JUMP
			{
				
				this.__nextState = 3; // JUMP
				ChangeState();
			}

			else if(this.__counter >= 0.2) // RUN
			{
				
				this.__nextState = 1; // RUN
				ChangeState();
			}
			else if(Input.GetButtonDown("Attack")) // ATTACK
			{
				this.ComboType = 1;
			}
			/////////////////////////////
			
		}

		//COMBO OUT
		else if (this.__nextState == 9)
		{
			//behavior//////////////////////
			this.xSpeed = 0;
			//////////////////////////////

			//Change State////////////////
			if(this.IsMoving)//RUN OPEN FAN
			{
				
				this.__nextState = 8; // RUN OPEN FAN
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Jump"))//JUMP
			{
				
				this.__nextState = 3; // JUMP
				ChangeState();
			}
			
			else if(Input.GetButtonDown ("Dodge"))//DODGE
			{
				this.__nextState = 2; // DODGE
				ChangeState();
			}
			
			else if(this.__counter >= 0.4) // STAND
			{
				this.__nextState = 0; // STAND
				ChangeState();
			}
			else if(Input.GetButtonDown("Attack")) // ATTACK
			{
				this.ComboType = 1;
			}
			//////////////////////////////
			
		}
		// DOUBLE JUMP
		else if(this.__nextState == 10)
		{
			if(!this.HasDoubleJumped)
			{
				this.HasDoubleJumped = true;
				this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1150);
			}
			//behavior//////////////////////
			this.FxAnimation = "fx_fulin_jump";
			PlayFootFX();
			this.xSpeed = this.xSpeed = (Input.GetAxis("Horizontal")* 500) * Time.deltaTime;
			///////////////////////////////
			
			//Change State////////////////
			if(this.GetComponent<Rigidbody2D>().velocity.y < 0)//FALL
			{
				this.__nextState = 4;//FALL
				ChangeState();
			}
			//////////////////////////////	
			
		}

		// AIR DODGE
		else if (this.__nextState == 11)
		{ 
			//behavior//////////////////////
			this.Rigidbody.velocity = new Vector2(this.Rigidbody.velocity.x,0);
			this.Rigidbody.gravityScale = 0;
			this.__duration = 0.3f;
			this.xSpeed = (__direction* 1500) * Time.deltaTime;
			///////////////////////////////
			
			//Change State//////////////////////
			if(this.__counter >= this.__duration)//FALL
			{
				
				this.__nextState = 4;//FALL
				ChangeState();
			}
			//////////////////////////////////
		}

		///////////////////////////////////////////////////////////////////COMBOS///////////////////////////////////////////////////////////////////////////////////////////////

		if (this.ComboType == 1) {
			this.__comboCounter += Time.deltaTime;
			this.Weapon.enabled = true;
				
			if (this.__comboState == 0) {
				if (this.__comboCounter >= 0.6) { // COMBO OUT
					this.__nextState = 9; // COMBO OUT
					ChangeState ();
				} else if (this.__comboCounter >= 0.4 && Input.GetButtonDown ("Attack")) { // COMBO OUT
					this.__comboState = 1; // STEP1
					this.__comboCounter = 0;
				}
			} else if (this.__comboState == 1) {
				if (this.__comboCounter >= 0.6) { // COMBO OUT
					this.__nextState = 9; // COMBO OUT
					ChangeState ();
				} else if (this.__comboCounter >= 0.4 && Input.GetButtonDown ("Attack")) { // COMBO OUT
					this.__comboState = 2; // STEP1
					this.__comboCounter = 0;
				}
			} else if (this.__comboState == 2) {
				if (this.__comboCounter >= 0.6) { // COMBO OUT
					this.__nextState = 9; // COMBO OUT
					ChangeState ();
				} else if (this.__comboCounter >= 0.4 && Input.GetButtonDown ("Attack")) { // COMBO OUT
					this.__comboState = 3; // STEP1
					this.__comboCounter = 0;
				}
			} else if (this.__comboState == 3) {
				if (this.__comboCounter >= 0.6) { // COMBO OUT
					this.__nextState = 9; // COMBO OUT
					ChangeState ();
				}

			}
		}
	}

}
