﻿using UnityEngine;
using System.Collections;

public class InputConfig : MonoBehaviour {
	[HideInInspector]
	public float JoystickMoveTolerance; // Minimum angle of stick to setup character's movement
	// Use this for initialization
	void Start () {
		this.JoystickMoveTolerance = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
